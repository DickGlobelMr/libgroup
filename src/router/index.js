import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/page/home'
import FirstLib from "../components/page/views/FirstLib"
import SecondLib from "../components/page/views/SecondLib"
import ThirdLib from "../components/page/views/ThirdLib"
import FirstSecondRel from "../components/page/views/FirstSecondRel"
import SecondThirdRel from "../components/page/views/SecondThirdRel"
import ServiceLib from "../components/page/views/ServiceLib"

Vue.use(Router)
export default new Router({
    hashbang: true,
    history: false,//这个参数改为false就可以了
    saveScrollPosition: true,
    suppressTransitionError: true,
    routes: [
        {
            path: "/",
            component: Home,
            children: [
                {
                    path: "/",
                    component: FirstLib
                },
                {
                    path: "/FirstLib",
                    component: FirstLib
                },
                {
                    path: "/SecondLib",
                    component: SecondLib
                },
                {
                    path: "/ThirdLib",
                    component: ThirdLib
                },
                {
                    path: "/FirstSecondRel",
                    component: FirstSecondRel
                },
                {
                    path: "/SecondThirdRel",
                    component: SecondThirdRel
                },
                {
                    path: "/ServiceLib",
                    component: ServiceLib
                },

            ]
        },
    ]
});
