import request from "@/utils/request"

// 查询业务
export function getService () {
    return request({
        url: '/businesss',
        method: 'get'
    })
}

// 新增业务
export function addService (data) {
    return request({
        url: '/business',
        method: 'post',
        data: data
    })
}

// 修改业务
export function updateService (data) {
    return request({
        url: '/business',
        method: 'put',
        data: data
    })
}

// 删除业务
export function deleteService (id) {
    return request({
        url: '/business/' + id,
        method: 'delete'
    })
}