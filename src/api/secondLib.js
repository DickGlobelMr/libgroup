import request from "@/utils/request"

// 查询二层
export function getSecond () {
    return request({
        url: '/smartbasicsseconds',
        method: 'get'
    })
}

// 新增二层
export function addSecond (data) {
    return request({
        url: '/smartbasicssecond',
        method: 'post',
        data: data
    })
}

// 修改二层
export function updateSecond (data) {
    return request({
        url: '/smartbasicssecond',
        method: 'put',
        data: data
    })
}

// 删除二层
export function deleteSecond (id) {
    return request({
        url: '/smartbasicssecond/' + id,
        method: 'delete'
    })
}