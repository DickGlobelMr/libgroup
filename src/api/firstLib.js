import request from "@/utils/request"

// 查询一层
export function getFirst () {
    return request({
        url: '/smartbasicss',
        method: 'get'
    })
}

// 新增一层
export function addFirst (data) {
    return request({
        url: '/smartbasics',
        method: 'post',
        data: data
    })
}

// 修改一层
export function updateFirst (data) {
    return request({
        url: '/smartbasics',
        method: 'put',
        data: data
    })
}

// 删除一层
export function deleteFirst (id) {
    return request({
        url: '/smartbasics/' + id,
        method: 'delete'
    })
}