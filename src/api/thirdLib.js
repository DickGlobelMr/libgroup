import request from "@/utils/request"

// 查询三层
export function getThird () {
    return request({
        url: '/smartbasicsthirds',
        method: 'get'
    })
}

// 新增三层
export function addThird (data) {
    return request({
        url: '/smartbasicsthird',
        method: 'post',
        data: data
    })
}

// 修改三层
export function updateThird (data) {
    return request({
        url: '/smartbasicsthird',
        method: 'put',
        data: data
    })
}

// 删除三层
export function deleteThird (id) {
    return request({
        url: '/smartbasicsthird/' + id,
        method: 'delete'
    })
}