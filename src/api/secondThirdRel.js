import request from "@/utils/request"

// 查询二层三层
export function getRel () {
    return request({
        url: '/smartbasicssecondthirds',
        method: 'get'
    })
}

// 新增二层三层
export function addRel (data) {
    return request({
        url: '/smartbasicssecondthird',
        method: 'post',
        data: data
    })
}

// 修改二层三层
export function updateRel (data) {
    return request({
        url: '/smartbasicssecondThird',
        method: 'put',
        data: data
    })
}

// 删除二层三层
export function deleteRel (id) {
    return request({
        url: '/smartbasicssecondthird/' + id,
        method: 'delete'
    })
}