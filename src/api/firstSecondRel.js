import request from "@/utils/request"

// 查询一层二层
export function getRel () {
    return request({
        url: '/smartbasicsfirstseconds',
        method: 'get'
    })
}

// 新增一层二层
export function addRel (data) {
    return request({
        url: '/smartbasicsfirstSecond',
        method: 'post',
        data: data
    })
}

// 修改一层二层
export function updateRel (data) {
    return request({
        url: '/smartbasicsfirstsecond',
        method: 'put',
        data: data
    })
}

// 删除一层二层
export function deleteRel (id) {
    return request({
        url: '/smartbasicsfirstsecond/' + id,
        method: 'delete'
    })
}